﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bibbleMP.Models;

namespace bibbleMP.Services
{
    public class MockPlayListDataStore : IDataStore<PlayList>
    {
        List<PlayList> playLists;

        public MockPlayListDataStore()
        {
            playLists = new List<PlayList>();
            var mockPlayLists = new List<PlayList>
            {
                new PlayList { Id = Guid.NewGuid().ToString(), Name = "12 Years on", Description="Married and happy." },
                new PlayList { Id = Guid.NewGuid().ToString(), Name = "16k Take 2", Description="On the run." },
                new PlayList { Id = Guid.NewGuid().ToString(), Name = "51 and a day", Description="Getting on." },
                new PlayList { Id = Guid.NewGuid().ToString(), Name = "Andy2015", Description="Andy 2015." },
            };

            foreach (var playList in mockPlayLists)
            {
                playLists.Add(playList);
            }
        }

        public async Task<bool> AddItemAsync(PlayList playList)
        {
            playLists.Add(playList);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(PlayList playList)
        {
            var oldItem = playLists.FirstOrDefault(arg => arg.Id == playList.Id);
            playLists.Remove(oldItem);
            playLists.Add(playList);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = playLists.FirstOrDefault(arg => arg.Id == id);
            playLists.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<PlayList> GetItemAsync(string id)
        {
            return await Task.FromResult(playLists.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<PlayList>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(playLists);
        }

    }
}
