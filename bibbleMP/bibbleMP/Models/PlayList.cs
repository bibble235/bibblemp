﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bibbleMP.Models
{
    public class PlayList
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
