﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bibbleMP.Models
{
    public enum MenuItemType
    {
        Browse,
        About,
        PlayLists
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
