﻿using System;
using System.Collections.Generic;
using System.Text;
using bibbleMP.Models;

namespace bibbleMP.ViewModels
{
    public class PlayListViewModel : BaseViewModel
    {
        public PlayList PlayList { get; set; }

        public PlayListViewModel(PlayList playlist = null)
        {
            Title = playlist?.Name;
            PlayList = playlist;
        }

    }
}
