﻿using bibbleMP.Models;

namespace bibbleMP.ViewModels
{
    public class PlayListDetailViewModel : BasePlayListViewModel
    {
        public PlayList PlayList { get; set; }
        public PlayListDetailViewModel(PlayList playList = null)
        {
            Title = playList?.Name;
            PlayList = playList;
        }
    }
}