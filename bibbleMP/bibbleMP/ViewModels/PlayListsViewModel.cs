﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using bibbleMP.Models;
using Xamarin.Forms;
using System.Threading.Tasks;
using bibbleMP.Services;
using bibbleMP.Views;

namespace bibbleMP.ViewModels
{
    public class PlayListsViewModel : BasePlayListViewModel
    {
        public ObservableCollection<PlayList> PlayLists { get; set; }
        public Command LoadPlayListsCommand { get; set; }

        public PlayListsViewModel()
        {
            Title = "Play Lists";

            PlayLists = new ObservableCollection<PlayList>();
            LoadPlayListsCommand = new Command(async () => await ExecuteLoadPlayListsCommand());
            
            MessagingCenter.Subscribe<NewPlayListPage, PlayList>(this, "AddPlayList", async (obj, playList) =>
            {
                var newPlayList = playList;
                PlayLists.Add(newPlayList);
                await DataStore.AddItemAsync(newPlayList);
            });
            
        }

        async Task ExecuteLoadPlayListsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                PlayLists.Clear();
                var items = await DataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    PlayLists.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }

        }
    }
}
