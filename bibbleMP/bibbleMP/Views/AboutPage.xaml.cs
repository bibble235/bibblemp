﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace bibbleMP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }
    }
}