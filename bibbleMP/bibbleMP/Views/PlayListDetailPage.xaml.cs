﻿using bibbleMP.Models;
using bibbleMP.ViewModels;
using Xamarin.Forms.Xaml;

namespace bibbleMP.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PlayListDetailPage
	{
	    PlayListDetailViewModel viewModel;

        public PlayListDetailPage(PlayListDetailViewModel viewModel)
		{
	        InitializeComponent();

	        BindingContext = this.viewModel = viewModel;
	    }

	    public PlayListDetailPage()
	    {
	        InitializeComponent();

	        var playList = new PlayList
	        {
	            Name = "Play List 1",
	            Description = "This is an item description."
	        };

	        viewModel = new PlayListDetailViewModel(playList);
	        BindingContext = viewModel;
	    }
	}
}