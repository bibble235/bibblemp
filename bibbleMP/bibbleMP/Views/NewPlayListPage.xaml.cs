﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using bibbleMP.Models;

namespace bibbleMP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewPlayListPage
    {
        public PlayList PlayList { get; set; }

        public NewPlayListPage()
        {
            InitializeComponent();

            PlayList = new PlayList
            {
                Name = "PlayItem name",
                Description = "This is an item description."
            };

            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "AddPlayList", PlayList);
            await Navigation.PopModalAsync();
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}